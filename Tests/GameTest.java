import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;

public class GameTest {

    @Test
    public void testWinGameAndGuessCheck(){
        Board testBoard = new Board(Peg.guessColor.Blue, Peg.guessColor.Green, Peg.guessColor.Purple, Peg.guessColor.Red);

        //Guess #1
        Guess badGuess = new Guess(Peg.guessColor.Yellow, Peg.guessColor.Yellow, Peg.guessColor.Yellow, Peg.guessColor.Yellow);
        testBoard.checkGuess(badGuess);

        Assert.assertTrue(badGuess.hintPegs.isEmpty());
        checkPlayingAndWon(true, false, testBoard);


        //Guess #2
        badGuess = new Guess(Peg.guessColor.Red, Peg.guessColor.Yellow, Peg.guessColor.Yellow, Peg.guessColor.Yellow);
        testBoard.checkGuess(badGuess);

        Assert.assertTrue(badGuess.hintPegs.size() == 1);
        Assert.assertTrue(!badGuess.hintPegs.get(0).isGuessPeg && !(badGuess.hintPegs.get(0).isRightSpot));
        checkPlayingAndWon(true, false, testBoard);


        //Guess #3
        badGuess = new Guess(Peg.guessColor.Yellow, Peg.guessColor.Yellow, Peg.guessColor.Yellow, Peg.guessColor.Red);
        testBoard.checkGuess(badGuess);

        Assert.assertTrue(badGuess.hintPegs.size() == 1);
        Assert.assertTrue(!badGuess.hintPegs.get(0).isGuessPeg && badGuess.hintPegs.get(0).isRightSpot);
        checkPlayingAndWon(true, false, testBoard);


        //Guess #4
        badGuess = new Guess(Peg.guessColor.Purple, Peg.guessColor.Yellow, Peg.guessColor.Yellow, Peg.guessColor.Red);
        testBoard.checkGuess(badGuess);

        Assert.assertTrue(badGuess.hintPegs.size() == 2);
        int whiteCount = 0;
        int redCount = 0;
        for(Peg thePeg:badGuess.hintPegs){
            Assert.assertFalse(thePeg.isGuessPeg);
            if(thePeg.isRightSpot)
                redCount++;
            else
                whiteCount++;
        }
        Assert.assertTrue(whiteCount == 1 && redCount == 1);
        checkPlayingAndWon(true, false, testBoard);


        //Guess #5
        badGuess = new Guess(Peg.guessColor.Yellow, Peg.guessColor.Yellow, Peg.guessColor.Purple, Peg.guessColor.Red);
        testBoard.checkGuess(badGuess);

        Assert.assertTrue(badGuess.hintPegs.size() == 2);
        whiteCount = 0;
        redCount = 0;
        for(Peg thePeg:badGuess.hintPegs){
            Assert.assertFalse(thePeg.isGuessPeg);
            if(thePeg.isRightSpot)
                redCount++;
            else
                whiteCount++;
        }
        Assert.assertTrue(whiteCount == 0 && redCount == 2);
        checkPlayingAndWon(true, false, testBoard);

        //Guess #6
        badGuess = new Guess(Peg.guessColor.Green, Peg.guessColor.Blue, Peg.guessColor.Purple, Peg.guessColor.Red);
        testBoard.checkGuess(badGuess);

        Assert.assertTrue(badGuess.hintPegs.size() == 4);
        whiteCount = 0;
        redCount = 0;
        for(Peg thePeg:badGuess.hintPegs){
            Assert.assertFalse(thePeg.isGuessPeg);
            if(thePeg.isRightSpot)
                redCount++;
            else
                whiteCount++;
        }
        Assert.assertTrue(whiteCount == 2 && redCount == 2);
        checkPlayingAndWon(true, false, testBoard);

        //Guess #7
        badGuess = new Guess(Peg.guessColor.Blue, Peg.guessColor.Green, Peg.guessColor.Purple, Peg.guessColor.Red);
        testBoard.checkGuess(badGuess);

        Assert.assertTrue(badGuess.hintPegs.size() == 4);
        whiteCount = 0;
        redCount = 0;
        for(Peg thePeg:badGuess.hintPegs){
            Assert.assertFalse(thePeg.isGuessPeg);
            if(thePeg.isRightSpot)
                redCount++;
            else
                whiteCount++;
        }
        Assert.assertTrue(whiteCount == 0 && redCount == 4);
        checkPlayingAndWon(false, true, testBoard);
    }

    @Test
    public void testLoseGame(){
        Board testBoard = new Board(Peg.guessColor.Blue, Peg.guessColor.Green, Peg.guessColor.Purple, Peg.guessColor.Red);

        //Guess #1
        Guess badGuess = new Guess(Peg.guessColor.Yellow, Peg.guessColor.Yellow, Peg.guessColor.Yellow, Peg.guessColor.Yellow);
        testBoard.checkGuess(badGuess);

        Assert.assertTrue(badGuess.hintPegs.isEmpty());
        checkPlayingAndWon(true, false, testBoard);


        //Guess #2-10
        for(int i = 0; i<9; i++){
            testBoard.checkGuess(badGuess);
            Assert.assertTrue(badGuess.hintPegs.isEmpty());
            Assert.assertFalse(testBoard.gameIsWon);
        }

        //Guess #11 (Not Valid)
        Guess correctGuess = new Guess(Peg.guessColor.Blue, Peg.guessColor.Green, Peg.guessColor.Purple, Peg.guessColor.Red);
        testBoard.checkGuess(correctGuess);

        Assert.assertTrue(correctGuess.hintPegs.size() == 4);
        int whiteCount = 0;
        int redCount = 0;
        for(Peg thePeg:correctGuess.hintPegs){
            Assert.assertFalse(thePeg.isGuessPeg);
            if(thePeg.isRightSpot)
                redCount++;
            else
                whiteCount++;
        }
        Assert.assertTrue(whiteCount == 0 && redCount == 4);
        checkPlayingAndWon(false, false, testBoard);

    }

    @Test
    public void testRandomAnswer(){
        Board testBoard = new Board();
        checkPlayingAndWon(true, false, testBoard);
        Guess testCorrectGuess = testBoard.answer;
        testCorrectGuess.hintPegs = new ArrayList<>();
        testBoard.checkGuess(testCorrectGuess);
        checkPlayingAndWon(false, true, testBoard);
    }

    private void checkPlayingAndWon(boolean playing, boolean won, Board theBoard){
        Assert.assertEquals(playing, theBoard.playingGame);
        Assert.assertEquals(won, theBoard.gameIsWon);
    }

    @Test
    public void testRandomThing(){
        System.out.println(Peg.guessColor.Red.toString());
    }
}