import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Peg {
    public enum guessColor{
        Red, Yellow, Green, Blue, Purple
    }

    public boolean isGuessPeg;
    public boolean isRightSpot;
    public guessColor pegColor;

    public Peg(guessColor color){
        isGuessPeg = true;
        pegColor = color;
    }

    public Peg(String color){
        isGuessPeg = true;
        pegColor = guessColor.valueOf(color);
    }

    public Peg(Boolean rightSpot){
        isGuessPeg = false;
        isRightSpot = rightSpot;
    }

    public static Peg getRandomColorPeg(){
        int pick = new Random().nextInt(guessColor.values().length);
        return new Peg(guessColor.values()[pick]);
    }

    public static String cycleStringColor(String currentColor){
        List<String> colorsList = new ArrayList<>();
        for(guessColor curColor: guessColor.values())
            colorsList.add(curColor.toString());
        int colorIdx = colorsList.indexOf(currentColor);
        colorIdx++;
        if(colorIdx >4)
            colorIdx = 0;
        String newColor = colorsList.get(colorIdx);

        return newColor;
    }
}
