import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

public class GameFrame extends JFrame {

    private JPanel contentPane;
    private JPanel guessingPane;
    private CircleButton color1;
    private CircleButton color2;
    private CircleButton color3;
    private CircleButton color4;
    private JButton submit;
    private JLabel remainingGuesses;
    private Board theGameBoard;

    public GameFrame(Board game){
        super("Mastermind 2410 Project");
        setLayout(new FlowLayout());
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setBounds(0, 0, 500, 1000);
        contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(5,5,5,5));
        contentPane.setLayout(new BorderLayout(0,0));
        contentPane.setBackground(new java.awt.Color(0, 191, 255));
        setContentPane(contentPane);

        drawTheBoard(game);
    }

    private void drawTheBoard(Board newGame){
        contentPane.removeAll();

        theGameBoard = newGame;

        guessingPane = new JPanel();
        guessingPane.setBackground(new java.awt.Color(87, 57, 29));
        guessingPane.setLayout(new GridLayout(14,1));
        contentPane.add(guessingPane, BorderLayout.CENTER);{

            JPanel remainingGuessesPanel = new JPanel();
            remainingGuessesPanel.setBackground(new java.awt.Color(255, 223, 0));
            remainingGuessesPanel.setLayout(new GridLayout(1,1));
            guessingPane.add(remainingGuessesPanel, BorderLayout.CENTER);{
                remainingGuesses = new JLabel("Remaining Guesses: 10");
                remainingGuessesPanel.add(remainingGuesses);
            }

            JPanel contain = new JPanel();
            contain.setBackground(new java.awt.Color(87, 57, 29));
            contain.setLayout(new GridLayout(1, 5));
            guessingPane.add(contain);{
                color1 = new CircleButton("Red", true);
                contain.add(color1);

                color2 = new CircleButton("Red", true);
                contain.add(color2);

                color3 = new CircleButton("Red",true);
                contain.add(color3);

                color4 = new CircleButton("Red",true);
                contain.add(color4);

                submit = new JButton("Submit");
                contain.add(submit);
            }

            JPanel guessDivider = new JPanel();
            guessDivider.setBackground(new java.awt.Color(255, 223, 0));
            guessDivider.setLayout(new GridLayout(1,1));
            guessingPane.add(guessDivider, BorderLayout.CENTER);{
                JLabel guessDividerText = new JLabel("--------Guesses--------");
                guessDivider.add(guessDividerText);
            }

        }

        actionHandler handler = new actionHandler();
        color1.addActionListener(handler);
        color2.addActionListener(handler);
        color3.addActionListener(handler);
        color4.addActionListener(handler);
        submit.addActionListener(handler);
    }

    private class actionHandler implements ActionListener{

        public void actionPerformed(ActionEvent event){
            if(event.getSource()==color1){
                String nextColorName = Peg.cycleStringColor(color1.getText());
                color1.setText(nextColorName);
                color1.setColor(nextColorName);
            }else if(event.getSource()==color2){
                String nextColorName = Peg.cycleStringColor(color2.getText());
                color2.setText(nextColorName);
                color2.setColor(nextColorName);
            }else if(event.getSource()==color3){
                String nextColorName = Peg.cycleStringColor(color3.getText());
                color3.setText(nextColorName);
                color3.setColor(nextColorName);
            }else if(event.getSource()==color4){
                String nextColorName = Peg.cycleStringColor(color4.getText());
                color4.setText(nextColorName);
                color4.setColor(nextColorName);
            }

            if(event.getSource()==submit){
                if(theGameBoard.playingGame) {
                    theGameBoard.checkGuess(new Guess(color1.getText(), color2.getText(), color3.getText(), color4.getText()));
                    remainingGuesses.setText("Remaining Guesses: " + (10 - theGameBoard.guesses.size()));
                    Guess recentGuess = theGameBoard.guesses.get(theGameBoard.guesses.size()-1);
                    showRecentGuess(recentGuess.guessPegs,recentGuess.hintPegs);
                    if(!theGameBoard.playingGame){
                        if(theGameBoard.gameIsWon)
                            showWinOrLoseAndShowReset("You Win!");
                        else
                            showWinOrLoseAndShowReset("You Lose!");
                    }
                }else{
                    drawTheBoard(new Board());
                    revalidate();
                }
            }
        }

        private void showRecentGuess(List<Peg> guessPegs, List<Peg> hintPegs){
            JPanel guess = new JPanel();
            guess.setBackground(new java.awt.Color(87, 57, 29));
            guess.setLayout(new GridLayout(1, 5));
            guessingPane.add(guess, BorderLayout.CENTER);{
                for(Peg guessPeg: guessPegs){
                    CircleButton guessCircle = new CircleButton(guessPeg.pegColor.toString());
                    guess.add(guessCircle);
                }
                JPanel hint = new JPanel();
                hint.setBackground(new java.awt.Color(87, 57, 29));
                hint.setLayout(new GridLayout(2, 2));
                guess.add(hint, BorderLayout.EAST);{
                    for(Peg hintPeg: hintPegs){
                        CircleButton hintCircle = new CircleButton(hintPeg.isRightSpot?"Right": "Same");
                        hintCircle.setColor(hintPeg.isRightSpot?"RED": "WHITE");
                        hint.add(hintCircle);
                    }
                }
            }
            revalidate();
        }

        private void showWinOrLoseAndShowReset(String message){
            JPanel endText = new JPanel();
            endText.setBackground(new java.awt.Color(255, 223, 0));
            endText.setLayout(new GridLayout(1,1));
            guessingPane.add(endText, BorderLayout.CENTER);{
                endText.add(new JLabel(message));
            }
            submit.setText("Reset");
        }
    }
}
