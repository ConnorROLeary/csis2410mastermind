public class Game {
    public static void main(String args[]) {
        Board theBoard = new Board();
        GameFrame theFrame = new GameFrame(theBoard);
        theFrame.setVisible(true);
    }
}
