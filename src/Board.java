import java.util.ArrayList;
import java.util.List;

public class Board {
    public List<Guess> guesses;
    public Guess answer;
    public boolean playingGame;
    public boolean gameIsWon;

    public Board(Peg.guessColor color1, Peg.guessColor color2, Peg.guessColor color3, Peg.guessColor color4){
        guesses = new ArrayList<>();
        answer = new Guess(color1, color2, color3, color4);
        playingGame = true;
        gameIsWon = false;
    }

    public Board(){
        guesses = new ArrayList<>();
        answer = randomizeAnswer();
        playingGame = true;
        gameIsWon = false;
    }

    public void checkGuess(Guess checkedGuess){
        checkedGuess.compareAndAddGuessPegs(answer);
        guesses.add(checkedGuess);
        checkIfGameOver();
    }

    public void checkIfGameOver(){
        if(gameIsWon || !playingGame)
            return;
        if(guesses.size() == 10){
            playingGame = false;
        }
        List<Peg> hintPegs = guesses.get(guesses.size()-1).hintPegs;
        boolean allRed = hintPegs.size()==4;
        if(allRed){
            for(Peg hint: hintPegs){
                if (!hint.isRightSpot){
                    allRed = false;
                }
            }
        }
        if(allRed){
            gameIsWon = true;
            playingGame = false;
        }
    }

    public Guess randomizeAnswer(){
        Guess theAnswer = new Guess();
        for(int i=0; i<4; i++){
            theAnswer.guessPegs.add(Peg.getRandomColorPeg());
        }

        return theAnswer;
    }
}
