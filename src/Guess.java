import java.util.*;

public class Guess {
    public List<Peg> guessPegs;
    public List<Peg> hintPegs;

    public Guess(Peg.guessColor color1, Peg.guessColor color2, Peg.guessColor color3, Peg.guessColor color4){
        guessPegs = new ArrayList<>();
        guessPegs.add(new Peg(color1));
        guessPegs.add(new Peg(color2));
        guessPegs.add(new Peg(color3));
        guessPegs.add(new Peg(color4));
        hintPegs = new ArrayList<>();
    }
    public Guess(String color1, String color2, String color3, String color4){
        guessPegs = new ArrayList<>();
        guessPegs.add(new Peg(color1));
        guessPegs.add(new Peg(color2));
        guessPegs.add(new Peg(color3));
        guessPegs.add(new Peg(color4));
        hintPegs = new ArrayList<>();
    }
    public Guess(){
        guessPegs = new ArrayList<>();
    }
    //This function will compare the guess and answer and add the corresponding hintPegs to the guess
    public void compareAndAddGuessPegs(Guess theAnswer){
        List<Peg.guessColor> answerColors = new ArrayList<>();
        for(Peg answerPeg: theAnswer.guessPegs){
            if(!answerColors.contains(answerPeg.pegColor)){
                answerColors.add(answerPeg.pegColor);
            }
        }
        int redCount = 0;
        int pegCount = 0;
        for(int i = 0; i<this.guessPegs.size(); i++){
            if(this.guessPegs.get(i).pegColor == theAnswer.guessPegs.get(i).pegColor){
                redCount++;
                pegCount++;

            }else if(answerColors.contains(this.guessPegs.get(i).pegColor)){
                pegCount++;
            }
        }
        for(int i = 0; i<pegCount; i++){
            if(i<redCount)
                this.hintPegs.add(new Peg(true));
            else
                this.hintPegs.add(new Peg(false));
        }
    }
}
